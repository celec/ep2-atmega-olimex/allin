#include <avr/io.h>

void write_char(unsigned char c, char *buff){
	if ((c >> 4) > 9) {buff[0] = (c >> 4) - 10 + 'A';}
	else {buff[0] = (c >> 4) + '0';} // dizaine
	if ((c & 0x0f) > 9) {buff[1] = (c & 0x0f) - 10 + 'A';}
	else {buff[1] = (c & 0x0f) + '0';} // unité
}

void write_short(unsigned short s, char *buff){
	write_char(s >> 8, &buff[0]); // millier et centaine
	write_char(s & 0xff, &buff[2]); // dizaine et unité
}
