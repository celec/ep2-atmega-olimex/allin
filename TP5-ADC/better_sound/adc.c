#include <avr/io.h>
#include <avr/interrupt.h>

void adc_init(void){
	ADMUX |= 1 << REFS0; // AREF = AVcc
	ADCSRA = (1 << ADEN) | (1 << ADPS2) | (1 << ADPS1) | (1 << ADPS0);
	// ADC Enable and prescaler of 128 : 16 MHz/128=125 kHz
	ADCSRA |= (1 << ADIE); // Enable ADC Interrupt
}
