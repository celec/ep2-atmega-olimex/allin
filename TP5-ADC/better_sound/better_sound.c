#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/power.h>
#include <avr/wdt.h>
#define F_CPU 16000000UL
#include "serial.h"
#include "adc.h"
#include "affiche.h"
#include <util/delay.h>

#define N 256

extern USB_ClassInfo_CDC_Device_t VirtualSerial_CDC_Interface;
extern FILE USBSerialStream;
volatile char flag = 0;

ISR(ADC_vect){flag = 1;}

int main(void){
	volatile unsigned short res[N], index = 0;
	char s[9];
	int k;

	SetupHardware();
	CDC_Device_CreateStream(&VirtualSerial_CDC_Interface, &USBSerialStream);
	GlobalInterruptEnable();
	adc_init();
	sei();

	s[0] = '0'; s[1] = 'x'; s[6] = '\r'; s[7] = '\n'; s[8] = 0;
	ADMUX |= 0x01; // ADC1
	ADCSRA |= (1 << ADSC); // start single conversion

	while(1){
		if (flag != 0) {
			res[index] = ADC;
			flag = 0; // fin de conversion par interrupt
			index++;
			if (index == N) {
				for (k = 0; k < N; k++){
					write_short(res[k], &s[2]);
					fputs(s, &USBSerialStream);
				}
				index = 0;
			}
			ADCSRA |= (1 << ADSC); // start single conversion
		}
					CDC_Device_USBTask(&VirtualSerial_CDC_Interface);
					USB_USBTask();
	}
	return 0;
}
