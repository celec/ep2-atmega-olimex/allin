#include <avr/io.h>
#define F_CPU 16000000UL
#include "serial.h"
#include "adc.h"
#include "affiche.h"
#include <util/delay.h>

extern USB_ClassInfo_CDC_Device_t VirtualSerial_CDC_Interface;
extern FILE USBSerialStream;

void gpio_init(void);

int main(void){
	unsigned short res = 0;
	char s[9];

	SetupHardware();
	CDC_Device_CreateStream(&VirtualSerial_CDC_Interface, &USBSerialStream);
	GlobalInterruptEnable();
	gpio_init();
	adc_init();

	s[0] = '0'; s[1] = 'x'; s[6] = '\r'; s[7] = '\n'; s[8] = 0;

	while(1){
		PORTB ^= 1 << PORTB5; PORTE ^= 1 << PORTE6;
		_delay_ms(500);
		res = adc_read(0);
		write_short(res, &s[2]);
		fputs(s, &USBSerialStream);

		// USB
		CDC_Device_ReceiveByte(&VirtualSerial_CDC_Interface);
		CDC_Device_USBTask(&VirtualSerial_CDC_Interface);
		USB_USBTask();
	}
	return 0;
}

void gpio_init(void){
	DDRB |= 1 << PORTB5;
	DDRE |= 1 << PORTE6;
	PORTB |= 1 << PORTB5;
	PORTE &= ~(1 << PORTE6);
}
