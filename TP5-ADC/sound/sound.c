#include <avr/io.h>
#define F_CPU 16000000UL
#include "serial.h"
#include "adc.h"
#include "affiche.h"
#include <util/delay.h>

extern USB_ClassInfo_CDC_Device_t VirtualSerial_CDC_Interface;
extern FILE USBSerialStream;

void gpio_init(void);

/* create sound: play -n synth sin 440 (sox package)
 * save data with cat /dev/ttyACM0 > data.dat
 * plot with octave via plt.m
 */
int main(void){
	unsigned short res[256];
	char s[9];
	int k;

	SetupHardware();
	CDC_Device_CreateStream(&VirtualSerial_CDC_Interface, &USBSerialStream);
	GlobalInterruptEnable();
	gpio_init();
	adc_init();

	s[0] = '0'; s[1] = 'x'; s[6] = '\r'; s[7] = '\n'; s[8] = 0;

	while(1){
		PORTB ^= 1 << PORTB5; PORTE ^= 1 << PORTE6;
		_delay_ms(100);
		for (k = 0; k < 256; k++) res[k] = adc_read(1);
		for (k = 0; k < 256; k++) {
			write_short(res[k], &s[2]);
			fputs(s, &USBSerialStream);
		}
		// USB
		CDC_Device_ReceiveByte(&VirtualSerial_CDC_Interface);
		CDC_Device_USBTask(&VirtualSerial_CDC_Interface);
		USB_USBTask();
	}
	return 0;
}

void gpio_init(void){
	DDRB |= 1 << PORTB5;
	DDRE |= 1 << PORTE6;
	PORTB |= 1 << PORTB5;
	PORTE &= ~(1 << PORTE6);
}
