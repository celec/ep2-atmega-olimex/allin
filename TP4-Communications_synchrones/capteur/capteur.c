#include <avr/io.h>
#include "serial.h" // LUFA
#include "begin.h" // init(), spi_init()
#include <util/delay.h>
#include "communication.h" // to_binary(), to_hex(), to_float(), send_byte()

#define cs_lo PORTC &= ~(1 << PORTC7)
#define cs_hi PORTC |= (1 << PORTC7) // CS#

extern USB_ClassInfo_CDC_Device_t VirtualSerial_CDC_Interface;
extern FILE USBSerialStream;

int main(void){
	char buffer[80] = "C'est parti\r\n\0";
	unsigned char ch, cl;
	int k;
	unsigned cc;

	init();
	spi_init();
	fputs(buffer, &USBSerialStream);

	while (1) {
		cs_lo; // active le périphérique

		// 8 premiers bits
		ch = send_byte(0x00);
		to_binary(ch, &buffer[0]); // 8 symboles
		to_hex(ch, &buffer[17]); // 2 symboles
		// 8 derniers bits
		cl = send_byte(0x00);
		to_binary(cl, &buffer[8]); // 8 symboles
		to_hex(cl, &buffer[19]); // 2 symboles

		for (k = 0; k < 2; k++){ send_byte(0x00); }
		cs_hi; // ferme le périphérique

		buffer[16] = '\t';
		buffer[21] = '\t';

		// concaténation du binaire pour conversion en float
		cc = ch; cc = cc << 8; cc |= cl;
		to_float(cc, &buffer[22]); // 6 symboles : 2 dizaines et 3 décimales
		// fin de la ligne
		buffer[29] = ' '; buffer[30] = '.'; buffer[31] = 'C';
		buffer[32] = '\r'; buffer[33] = '\n'; buffer[34] = 0;
		fputs(buffer, &USBSerialStream); // affichage port série
		_delay_ms(100);

		CDC_Device_ReceiveByte(&VirtualSerial_CDC_Interface);
		CDC_Device_USBTask(&VirtualSerial_CDC_Interface);
		USB_USBTask();
	}
	return 0;
}
