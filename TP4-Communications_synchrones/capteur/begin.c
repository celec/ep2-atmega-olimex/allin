#include <avr/io.h>
#define F_CPU 16000000UL
#include "serial.h"

extern USB_ClassInfo_CDC_Device_t VirtualSerial_CDC_Interface;
extern FILE USBSerialStream;

void init(void){
	SetupHardware();
	CDC_Device_CreateStream(&VirtualSerial_CDC_Interface, &USBSerialStream);
	GlobalInterruptEnable();
	DDRB |= 1 << PB4;
	PORTB &= (~1 << PB4);
}

void spi_init(void){
	// MOSI (B2), SCK (B1) en sortie, MISO (B3) en entree
	DDRB |= ((1<<DDB0)|(1 << DDB2)| (1 << DDB1));
	DDRB &= ~(1 << DDB3); // DDB0 doit etre out pour ne pas bloquer SPI
	DDRC |= (1 << DDC7); // CS#
	// Int Ena | SPI ENA | 0=MSB 1st | Master | CK idle hi | sample trailing SCK | f_OSC / 128
	SPCR = (0<<SPIE) | (1<<SPE) | (0<<DORD) | (1<<MSTR) | (0 << CPOL) | (0<<CPHA) | (1<<SPR1) | (1<<SPR0);
	SPSR &= ~(1 << SPI2X); // No doubled clock frequency
}
