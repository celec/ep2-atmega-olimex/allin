#include <avr/io.h>

char send_byte(char b){
	SPDR = b; // emet MOSI
	while(!(SPSR & (1 << SPIF)));
	SPSR &= ~(1 << SPIF);
	return SPDR; // renvoie MISO
}

void to_binary(unsigned char c, char *o){
	for (int i = 0; i < 8; i++){
		*(o + 7 - i) = ((c >> i) & 0x01) + '0';
	}	
}

void to_hex(unsigned char c, char *o){
	if ((c >> 4) > 9) {*o = (c >> 4) - 10 + 'A';}
	else {*o = (c >> 4) + '0';} // dizaine
	if ((c & 0x0f) > 9) {*(o + 1) = (c & 0x0f) - 10 + 'A';}
	else {*(o + 1) = (c & 0x0f) + '0';} // unité
}

void to_float(unsigned c, char *o){
	// hex * 0.0625 / 8 ou >> 3
	float deg;
	unsigned e, d, i;
	if (((c >> 15) & 0x01) == 1) {
		c ^= 1; c++; // si négatif
		*o = '-';
	} else {*o = ' ';}
	deg = (float)(c >> 3) * 0.0625; // conversion
	
	e = (unsigned)(deg) & 0xff; i = (unsigned)(e / 10);
	*(o + 1) = i + '0'; // dizaine
	*(o + 2) = e - i*10 + '0'; // unité
	
	*(o + 3) = '.';

	d = (unsigned)((deg - (float)e) * 1000);
	i = (unsigned)(d / 100);
	*(o + 4) = i + '0'; // dixième
	d = d - i * 100; i = (unsigned)(d / 10);
	*(o + 5) = i + '0'; // centième
	*(o + 6) = d - i*10 + '0'; // millième
}
