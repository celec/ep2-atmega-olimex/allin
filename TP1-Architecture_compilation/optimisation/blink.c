#include <avr/io.h>
#define F_CPU 16000000UL

void delai(long duree){
	for (volatile long k = 0; k < duree; k++){};
}

int main(void){
	DDRB |= 1 << PORTB5;
	DDRE |= 1 << PORTE6;
	PORTB |= 1 << PORTB5;
	PORTE &= ~(1 << PORTE6);

	while(1){
		PORTB ^= 1 << PORTB5; PORTE ^= 1 << PORTE6;
		delai(0xffff);
	}
	return 0;
}
