#include <avr/io.h>
#include <avr/wdt.h>
#include <avr/power.h>
#include <avr/interrupt.h>
#include <string.h>
#include <stdio.h>

#include "serial.h"
#include <util/delay.h>

extern USB_ClassInfo_CDC_Device_t VirtualSerial_CDC_Interface;
extern FILE USBSerialStream;

int main(void)
{
 SetupHardware();
/* Create a regular character stream for the interface so that it can be used with the stdio.h functions */
  CDC_Device_CreateStream(&VirtualSerial_CDC_Interface, &USBSerialStream);
  GlobalInterruptEnable();

  while (1) {
     fprintf(&USBSerialStream,"Char: %uo\nShort: %uo\nInt: %uo\nLong: %uo\r\n", sizeof(char), sizeof(short), sizeof(int), sizeof(long));
     //fprintf(&USBSerialStream, "Hexa:\t%x\nDec:\t%u\r\n", 0x342, 0x342); 

//   les 3 lignes ci-dessous pour accepter les signaux venant du PC 
     CDC_Device_ReceiveByte(&VirtualSerial_CDC_Interface);
     CDC_Device_USBTask(&VirtualSerial_CDC_Interface);
     USB_USBTask();

     _delay_ms(500);
    }
}
