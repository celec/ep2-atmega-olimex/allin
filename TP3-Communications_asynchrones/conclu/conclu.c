#include <avr/io.h>
#include <avr/interrupt.h>
#include "declarations.h"

const char buff[17] = "Hello, world!\r\n\0";

ISR(TIMER1_COMPA_vect){
	PORTB ^= 1 << PORTB5;
	PORTE ^= 1 << PORTE6;
}

ISR(INT2_vect){
	cli();
	transmit_str(buff, 17);
	sei();
}

int main(void){
	init();
	timer_init();
	uart_init();

	sei();
	USBCON = 0;

	while(1){};
	return 0;
}
