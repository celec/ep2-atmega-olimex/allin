#include <avr/io.h>
#include <avr/interrupt.h>
#define F_CPU 16000000UL

#define USART_BAUDRATE (9600)
#define delai (16000*2)

void transmit(char data){
	while (!(UCSR1A & (1 << UDRE1)));
	UDR1 = data;
}

void transmit_str(char *B, unsigned char len){
	for (unsigned char i = 0; i < len; ++i){
		transmit(B[i]);
	}
}

void init(void){
	DDRB |= 1 << PORTB5;
	DDRE |= 1 << PORTE6;
	PORTB |= 1 << PORTB5;
	PORTE &= ~(1 << PORTE6);

	// Bouton interruption
	DDRD &= ~(1 << PORTD2); // D0 relié à D11
	PORTD |= 1 << PORTD2; // Pull-Up
	PIND &= ~(1 << PORTD2);

	// Interruption
	MCUCR &= ~(1 << PUD);
	EIMSK = 1 << INT2; // enable INT2 only
}

void timer_init(void){ // Timer 1 (16b) avec prescaler 64 et CTC
	TCCR1A = 0;
	TCCR1B = (1 << WGM12) | (1 << CS11) | (1 << CS10);
	TCNT1 = 0;
	OCR1A = delai;
	TIMSK1 = 1 << OCIE1A;
}

void uart_init(void){
	UCSR1A = 0;
	UCSR1B = 0;
	UCSR1B = (1 << RXEN1) | (1 << TXEN1); // RX and TX enable
	UCSR1B |= 1 << RXCIE1; // enable interrupt RX complete
	UCSR1C = _BV(UCSZ11) | _BV(UCSZ10); // 8N1

	unsigned short baud = F_CPU / (USART_BAUDRATE*16UL - 1);
	UBRR1H = (unsigned char) (baud >> 8);
	UBRR1L = (unsigned char) baud;
}
