#include <avr/io.h>
#include "declarations.h"
#include <util/delay.h>

int main(void){
	unsigned char c;
	init();
	uart_init();

	while(1){
		c = received();
		PORTB ^= 1 << PORTB5; PORTE ^= 1 << PORTE6; // cligno
		(c >= 127) ? c = 42 : c++;
		transmit(c);
	}
	return 0;
}
