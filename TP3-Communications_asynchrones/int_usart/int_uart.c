#include <avr/io.h>
#include <avr/interrupt.h>
#include "declarations.h"
#include <util/delay.h>

volatile char r, flag = 0;

ISR(USART1_RX_vect){
	r = UDR1;
	flag = 1;
}

int main(void){
	init();
	uart_init();

	while(1){
		PORTB ^= 1 << PORTB5; PORTE ^= 1 << PORTE6; // cligno
		_delay_ms(500);
		if (flag != 0) {
			flag ^= 1;
			(r >= 127) ? r = 42 : r++;
			transmit(r);
		}
	}
	return 0;
}
