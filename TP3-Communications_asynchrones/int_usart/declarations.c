#include <avr/io.h>
#include <avr/interrupt.h>
#define F_CPU 16000000UL
#include <util/delay.h>

#define USART_BAUDRATE (9600)

void transmit(char data){
	while (!(UCSR1A & (1 << UDRE1)));
	UDR1 = data;
}

void init(void){
	DDRB |= 1 << PORTB5;
	DDRE |= 1 << PORTE6;
	PORTB |= 1 << PORTB5;
	PORTE &= ~(1 << PORTE6);
}

void uart_init(void){
	UCSR1A = 0;
	UCSR1B = 0;
	UCSR1B = (1 << RXEN1) | (1 << TXEN1); // RX and TX enable
	UCSR1B |= 1 << RXCIE1; // enable interrupt RX complete
	UCSR1C = _BV(UCSZ11) | _BV(UCSZ10); // 8N1

	unsigned short baud = F_CPU / (USART_BAUDRATE*16UL - 1);
	UBRR1H = (unsigned char) (baud >> 8);
	UBRR1L = (unsigned char) baud;

	USBCON = 0;
	sei();
}
