#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/power.h>
#include <avr/wdt.h>
#define F_CPU 16000000UL
#include "serial.h"
#include <util/delay.h>

#define delai 32000

extern USB_ClassInfo_CDC_Device_t VirtualSerial_CDC_Interface;
extern FILE USBSerialStream;

void transmit_data(uint8_t data){
	char buffer[2];
	buffer[0] = data; buffer[1] = 0;
	fputs(buffer, &USBSerialStream);
}

void init(void){
	DDRB |= 1 << PORTB5;
	PORTB |= 1 << PORTB5;

	_delay_ms(1000);
	
	//wdt_enable(WDTO_120MS);

	USBCON = 0;
	SetupHardware();
	CDC_Device_CreateStream(&VirtualSerial_CDC_Interface, &USBSerialStream);
	GlobalInterruptEnable();
}
