#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/power.h>
#include <avr/wdt.h>
#define F_CPU 16000000UL
#include "declarations.h"
#include "serial.h"
#include <util/delay.h>

extern USB_ClassInfo_CDC_Device_t VirtualSerial_CDC_Interface;
extern FILE USBSerialStream;

int main(void){
	init();
	transmit_data('.');
	//wdt_enable(WDTO_120MS);
	while(1){
		wdt_reset();
		PORTB &= ~(1 << PORTB5);
		_delay_ms(20);
		transmit_data('.');
		transmit_data('\r');
		transmit_data('\n');

		CDC_Device_ReceiveByte(&VirtualSerial_CDC_Interface);
		CDC_Device_USBTask(&VirtualSerial_CDC_Interface);
		USB_USBTask();
	}
	return 0;
}
