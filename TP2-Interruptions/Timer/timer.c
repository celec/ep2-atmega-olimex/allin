#include <avr/io.h>
#include <avr/interrupt.h>
#include "declarations.h"

volatile int c = 0;

ISR(TIMER1_OVF_vect) {
	PORTE ^= 1<<PORTE6;
}

ISR(TIMER0_OVF_vect){
	c++;
	if (c == 5){ //modulo 6 : T = 5*16ms ~ 82ms
		PORTB ^= 1<<PORTB5;
		c = 0;
	}
}

int main(void)
{
	timer_init();
	init();

	while(1){}

	return 0;
}
