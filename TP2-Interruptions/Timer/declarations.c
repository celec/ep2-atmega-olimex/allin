#include <avr/io.h>
#include <avr/interrupt.h>
#define F_CPU 16000000UL

void timer_init(){
	// Timer 1 : 16b et prescale 64 --> diode D7 (verte)
	// Periode T = 0xffff * 64 / F_CPU (overflow) = 260ms
	TCCR1B = (1 << CS11) | (1 << CS10);
	TIMSK1 = 1 << TOIE1;

	// Timer 0 : 8b et prescale 1024 --> diode D9 (jaune)
	// Periode T = 0xff * 1024 / F_CPU (overflow) = 16ms
	TCCR0B = (1 << CS02) | (1 << CS00);
	TIMSK0 = 1 << TOIE0;
}

void init(){
	DDRB |=1<<PORTB5;
	PORTB |= 1<<PORTB5; // D9 init à 1
	DDRE |=1<<PORTE6;
	PORTE &=~1<<PORTE6;

	USBCON=0;
	sei();
}
