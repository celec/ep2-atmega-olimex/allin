#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/power.h>
#include <avr/wdt.h>
#define F_CPU 16000000UL
#define delai (16000*2)

void timer_init(){ // Timer 1 (16b) avec prescaler 64 et CTC
	TCCR1A = 0;
	TCCR1B = (1 << WGM12) | (1 << CS11) | (1 << CS10);
	TCNT1 = 0;
	OCR1A = delai;
	// T = delai * 64 / F_CPU
	TIMSK1= (1 << OCIE1A);
	sei();
}

void init(){
	USBCON = 0;

	DDRB |= 1<<PORTB5;
	PORTB |= 1<<PORTB5; // D9 init à 1
	DDRE |= 1<<PORTE6;
	PORTE &= (~1<<PORTE6); // D7 init à 0
}
