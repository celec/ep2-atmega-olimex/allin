#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/power.h>
#include <avr/wdt.h>
#include "declarations.h"

ISR(TIMER1_COMPA_vect) {
	PORTB ^= 1 << PORTB5;
	PORTE ^= 1 << PORTE6;
}

int main(void)
{
	init();
	timer_init();

	while(1){}

	return 0;
}
