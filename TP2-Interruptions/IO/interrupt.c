#include <avr/io.h>
#include <avr/interrupt.h>
#include "declarations.h" // affiche & init
#define F_CPU 16000000UL
#include <util/delay.h>
volatile char sens = 1;

ISR(INT2_vect){
	sens ^= 1; // flag changement de sens
	cli();
}

int main(void){
	init();
	unsigned char x = 1;
	char flag = sens;

	while(1){
		if (sens != flag){
			flag ^= 1;
			sei();
		}
		if (sens) { // sens positif
			if (x > 9) {x = 1;}
			(x != 1) ? affiche(x-1, 0) : affiche(9, 0);
			affiche(x, 1); 
			x++;
		} else { // sens inverse
			if (x < 1) {x = 9;}
			(x != 9) ? affiche(x+1, 0): affiche(1, 0);
			affiche(x, 1);
			x--;
		}
		_delay_ms(200);
	}
	return 0;
}
