#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/power.h>
#include "declarations.h"
volatile unsigned char x = 0;

ISR(TIMER1_COMPA_vect) {
	if (x > 9) {x = 0;}
	(x != 0) ? affiche(x-1, 0) : affiche(9, 0);
	affiche(x, 1);
	x++;
}

int main(void)
{
	init();
	timer_init();

	while(1){}

	return 0;
}
