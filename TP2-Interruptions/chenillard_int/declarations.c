#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/power.h>
#define F_CPU 16000000UL
#define delai (16000*2)

typedef unsigned char uchar;
uchar broche[10] = {2,3,1,0,4,6,7,6,4,5}; //n of PORTXn
uchar port[10] = {2,2,2,2,2,1,2,3,0,0}; // B=0, then +3

// ignite or not (bool e) the i-esth LED
void affiche(uchar i, char e){
	(e) ? (*(uchar*)(0x25 + 3*port[i]) |= 1<<broche[i]) : (*(uchar*)(0x25 + 3*port[i]) &= ~(1<<broche[i]));
}

void timer_init(){ // Timer 1 (16b) avec prescaler 64 et CTC
	TCCR1A = 0;
	TCCR1B = (1 << WGM12) | (1 << CS11) | (1 << CS10);
	TCNT1 = 0;
	OCR1A = delai;
	// T = delai * 64 / F_CPU
	TIMSK1 = 1 << OCIE1A;
	sei();
}

void init(){
	USBCON = 0;
	
	// Déclarations des registres
	for (uchar i = 0; i < 10; ++i) { // sorties et mise à 0
		*(uchar*)(0x24 + 3*port[i]) |= 1<<broche[i];
		*(uchar*)(0x25 + 3*port[i]) &= ~(1<<broche[i]);
	}
}
