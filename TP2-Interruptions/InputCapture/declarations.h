void transmit_data(uint8_t);
void write_char(unsigned char);
void write_short(unsigned short);
void icp_setup(void);
void pwm_setup(void);
void init(void);
