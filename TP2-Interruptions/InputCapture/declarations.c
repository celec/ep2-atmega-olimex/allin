#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/power.h>
#define F_CPU 16000000UL
#include "serial.h"
#include <util/delay.h>

#define delai 32000

extern USB_ClassInfo_CDC_Device_t VirtualSerial_CDC_Interface;
extern FILE USBSerialStream;

void transmit_data(uint8_t data){
	char buffer[2];
	buffer[0] = data; buffer[1] = 0;
	fputs(buffer, &USBSerialStream);
}

void write_char(unsigned char c){
	((c >> 4) > 9) ? transmit_data((c >> 4) - 10 + 'A') : transmit_data((c >> 4) + '0'); // dizaine
	((c & 0xf) > 9) ? transmit_data((c & 0xf) - 10 + 'A') : transmit_data((c & 0xf) + '0'); // unité
}

void write_short(unsigned short s){
	write_char(s >> 8); // millier et centaine
	write_char(s & 0xff); // dizaine et unité
}

void icp_setup(void){ // timer 3
	TCCR3A = (1 << WGM31) | (1 << WGM30);
	TCCR3B = (1 << ICES3) | (1 << WGM33) | (1 << CS31);
	OCR3A = delai;
	TIMSK3 = (1 << ICIE3);
	TIFR3 = (1 << ICF3);
}

void pwm_setup(void){ // créneau
	DDRB |= (1<<7)|(1<<6)|(1<<5); // B5/OC1A, B6/OC1B, B7/OC1C
	ICR1 = 0x3fff; // 0x3e80 : 1ms -> période du créneau
	TCCR1A = 0xaa;
	TCCR1B = 0x19;
	OCR1A = 0x3fff / 3; // 0x3e80/2 : 500us -> largeur du créneau (τ = 50%)
}

void init(void){
	DDRB |= 1 << PORTB5;
	DDRE |= 1 << PORTE6;
	PORTB |= 1 << PORTB5;
	PORTE &= ~(1 << PORTE6);
	
	pwm_setup();
	icp_setup();
	
	sei();
	USBCON = 0;
	SetupHardware();
	CDC_Device_CreateStream(&VirtualSerial_CDC_Interface, &USBSerialStream);
	GlobalInterruptEnable();
}
