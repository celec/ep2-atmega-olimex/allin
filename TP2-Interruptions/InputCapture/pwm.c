#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/power.h>
#define F_CPU 16000000UL
#include "declarations.h"
#include "serial.h"
#include <util/delay.h>

volatile unsigned short res;
extern USB_ClassInfo_CDC_Device_t VirtualSerial_CDC_Interface;
extern FILE USBSerialStream;

ISR(TIMER3_CAPT_vect){
	res = ICR3;
	TCNT3 = 0;
}

int main(void){
	init();

	while(1){
		if (res != 0) {
			write_short(res);
			transmit_data(0x0A);
			transmit_data(0x0D);
			res = 0;
		}

		CDC_Device_ReceiveByte(&VirtualSerial_CDC_Interface);
		CDC_Device_USBTask(&VirtualSerial_CDC_Interface);
		USB_USBTask();
	}
	return 0;
}
